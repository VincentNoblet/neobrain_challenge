from selenium import webdriver
import pandas as pd
from bs4 import BeautifulSoup
import hashlib
import d6tflow
import luigi
import json
from collections import namedtuple
import re
from tqdm import tqdm
from pathlib import Path
from selenium.webdriver.common.by import By

INDEED_METADATA = {
    "OFFER_PER_PAGE": 10,
    "COUNTRY": {
        "USA": {"location": "United States", "url": "indeed.com", "job_search_count": "Page (.*) of (.*) jobs"},
        "FRA": {"location": "France", "url": "indeed.fr", "job_search_count": "Page (.*) de (.*) emplois"}
    }
}
INDEED_METADATA_OBJECT = json.loads(str(INDEED_METADATA).replace("'", "\""), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))


class TaskScrapIndeedJobOffer(d6tflow.tasks.TaskCSVPandas):
    job_title = luigi.parameter.Parameter()
    country = luigi.parameter.Parameter()
    strategy = luigi.parameter.Parameter()

    def __init__(self, **dict_params):
        strategy_param = dict_params["strategy"]

        if strategy_param == "all":
            self.scrapping_strategy = self.strategy_all
        elif strategy_param == "listing":
            self.scrapping_strategy = self.strategy_listing

        elif strategy_param == "listing_requirements":
            raise ValueError("This strategy is not implemented yet")
        else:
            raise ValueError("This strategy does not exists")
        super(TaskScrapIndeedJobOffer, self).__init__(**dict_params)

    def strategy_all(self, element):
        return element.text

    def strategy_listing(self, element):
        return "\n".join([e.text for e in element.find_elements(By.TAG_NAME, 'li')])

    def get_job_search_count(self, driver):
        total_count = re.match(INDEED_METADATA["COUNTRY"][self.country]["job_search_count"], driver.find_element_by_id("searchCountPages").text).group(2)
        return int("".join([c for c in list(total_count) if c not in [" ", ","]]))

    def get_object_from_dict(self, d): return json.loads(str(d).replace("'", "\""), object_hook=lambda _d: namedtuple('X', _d.keys())(*d.values()))

    def get_job_description_element(self, driver):
        try:
            job_desc_element = driver.find_element_by_id("vjs-content")
        except:
            job_desc_element = driver.find_element_by_id("jobDescriptionText")
        return job_desc_element

    def run(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")
        driver = webdriver.Chrome("./chromedriver", options=options)
        country = self.get_object_from_dict(INDEED_METADATA["COUNTRY"][self.country])
        driver.get("https://{url}/jobs?q={job_title}&sort=date&l={location}&start={start}".format(
                                                                                             url=country.url,
                                                                                             job_title=self.job_title,
                                                                                             location=country.location,
                                                                                             start=0))
        progress_bar_print = "Scrapping Indeed {job_title} offers in {location}".format(job_title=self.job_title,
                                                                                        location=country.location)
        df = pd.DataFrame()
        max_loop = min(self.get_job_search_count(driver), 800)
        for i, start in enumerate(tqdm(range(0, max_loop, INDEED_METADATA_OBJECT.OFFER_PER_PAGE), desc=progress_bar_print)):
            driver.get("https://{url}/jobs?q={job_title}&sort=date&l={location}&start={start}".format(
                                                                                                 url=country.url,
                                                                                                 job_title=self.job_title,
                                                                                                 location=country.location,
                                                                                                 start=start))
            driver.implicitly_wait(2)
            all_jobs = driver.find_elements_by_class_name("result")

            for job in all_jobs:
                try:
                    result_html = job.get_attribute('innerHTML')
                    soup = BeautifulSoup(result_html, "html.parser")
                    try:
                        title = soup.find("a", class_="jobtitle").text.replace("\n", "")
                    except:
                        title = None

                    sum_div = job.find_elements_by_class_name("summary")[0]
                    try:
                        driver.execute_script("arguments[0].scrollIntoView();", sum_div)
                        sum_div.click()
                    except:
                        close_button = driver.find_element_by_class_name("popover-x-button-close")
                        close_button.click()
                        driver.execute_script("arguments[0].scrollIntoView();", sum_div)
                        sum_div.click()

                    job_desc_element = self.get_job_description_element(driver)

                    job_desc = self.scrapping_strategy(job_desc_element)

                    df = df.append({"title": title, "description": job_desc,
                                    "hash_description": hashlib.sha256(job_desc_element.text.encode('utf-8')).hexdigest()},
                                   ignore_index=True)
                except:
                    print("Error: description job not found")
            if (i + 1) % 5 == 0:
                df.to_csv(Path(d6tflow.dirpath / self.get_task_family() / (self.task_id + "_temp.csv")))
        self.save(df)
